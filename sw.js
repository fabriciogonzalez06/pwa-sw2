// Ciclo de vida del SW


self.addEventListener('install', (event) => {

    //descargamos assets
    //Creamos cache

    let instalacion = new Promise((resolve, reject) => {


        setTimeout(() => {
            console.log("instalaciones terminadas");
            self.skipWaiting();
            resolve();
        }, 1);

    });

    //waitUntil espera a que se resuelva lo que tiene en parametros para continuar
    event.waitUntil(instalacion);

});

//cuando el service worker toma el control de la aplicación 
self.addEventListener('activate', (event) => {

    //borrar cache viejo

    console.log("activo y listo para controllar al app");
});


//Manejo de peticiones HTTP
self.addEventListener('fetch', (event) => {

    //aquí se utilizan las estrategias del cache
    console.log(event.request.url);

    if (event.request.url.includes('https://reqres.in/')) {

        let resp = new Response(`{ok:false, mensaje:'jajaj' }`);
        event.respondWith(resp);
    }
});


//SW Sync: Recuperamos la conexion a internet

self.addEventListener('sync', (event) => {

    console.log("tenemos conexion");
    console.log(event);
    console.log(event.tag);
});


//SW push: para manejo de notificaciones
self.addEventListener('push', (event) => {

    console.log(event);
});