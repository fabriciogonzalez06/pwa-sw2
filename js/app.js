// Detectar si podemos usar Service Workers
if (navigator.serviceWorker) {
    navigator.serviceWorker.register('/sw.js').then(reg => {

        setTimeout(() => {

            //registrar tarea asyncona
            reg.sync.register('posteo-gatitos');
            console.log("se envio posteo de gatitos al server");
        }, 3000)

        Notification.requestPermission().then(result => {
            console.log(result);
            reg.showNotification('HOla mundo');
        });
    });


}


/* fetch('https://reqres.in/api/users').then(resp => resp.text())
    .then(console.log); */